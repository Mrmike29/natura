@extends('layouts.admin')

@section('content')

    <?php $globalUser = Auth::user(); ?>

    <div class="right_col" role="main">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Crear Evento</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div id="container_message">

                        </div>
                        <br />
                        <form class="form-horizontal form-label-left" id="create_event" method="POST" action="{{ route('createEvent') }}" enctype="multipart/form-data">
                            @csrf
                            <?php if($globalUser['id_role'] == 1){ ?>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Gerencia <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select class="form-control" name="id_gerencia" id="id_gerencia">
                                        @foreach($idGerencia AS $item)
                                            <option value="{{ $item->id }}">{{ $item->nombre }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Tipo de Evento <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select class="form-control" name="tipo_evento" id="tipo_evento">
                                        <option value="1">Evento Presencial</option>
                                        <option value="2">Evento Virtual</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nombre del Evento <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input class="form-control col-md-7 col-xs-12" type="text" name="nombre_evento" id="nombre_evento" placeholder="Nombre del Evento">
                                    <?php if($globalUser['id_role'] != 1){ ?>
                                    <input type="hidden" name="id_gerencia" id="id_gerencia" value="{{$idGerencia}}">
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Fecha del Evento <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input class="form-control col-md-7 col-xs-12" type="date" name="fecha_evento" id="fecha_evento" placeholder="Fecha del Evento">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Hora del Evento <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input class="form-control col-md-7 col-xs-12" type="time" name="hora_evento" id="hora_evento" placeholder="Hora del Evento">
                                </div>
                            </div>
                            <div class="form-group" id="container_cupos">
                                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Máximo de Cupos <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input class="form-control col-md-7 col-xs-12" type="number" name="cupos" id="cupos" placeholder="Máximo de Cupos">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Número Contacto <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input class="form-control col-md-7 col-xs-12" type="number" name="numero_contacto" id="numero_contacto" placeholder="Número Contacto">
                                </div>
                            </div>
                            <div class="form-group" id="container_link" style="display: none">
                                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Link Evento <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input class="form-control col-md-7 col-xs-12" type="text" name="link" id="link" placeholder="Link Evento" >
                                </div>
                            </div>
                            <div class="form-group" id="container_fecha_habilitar_link" style="display: none">
                                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Fecha de Habilitación del Link <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input class="form-control col-md-7 col-xs-12" type="datetime-local" name="fecha_habilitar_link" id="fecha_habilitar_link" placeholder="Fecha de Habilitación del Link">

                                </div>
                            </div>
                            <div class="form-group" id="container_link_pedido" style="display: none">
                                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Link Para Ordenador de Pedido <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input class="form-control col-md-7 col-xs-12" type="text" name="link_pedido" id="link_pedido" placeholder="Link Para Ordenador de Pedido">
                                </div>
                            </div>
                            <div class="form-group" id="container_fecha_habilitar_link_pedido" style="display: none">
                                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Fecha de Habilitación del Link Para Ordenador de Pedido <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input class="form-control col-md-7 col-xs-12" type="datetime-local" name="fecha_habilitar_link_pedido" id="fecha_habilitar_link_pedido" placeholder="Fecha de Habilitación del Link Para Ordenador de Pedido">
                                </div>
                            </div>
                            <div class="form-group" id="container_lugar_evento">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Lugar del Evento <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea class="form-control" name="lugar_evento" id="lugar_evento" placeholder="Lugar del Evento"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Descripción <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea class="form-control" name="descripcion" id="descripcion" placeholder="Descripción"></textarea>
                                </div>
                            </div>

                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-primary" id="submit_button">Guardar</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script type="application/javascript">


        $('#tipo_evento').change(function () {
            if($(this).val()*1 === 1){
                $('#container_cupos').removeAttr('style');
                $('#container_lugar_evento').removeAttr('style');
                $('#container_link').css('display', 'none');
                $('#container_fecha_habilitar_link').css('display', 'none');
                $('#container_link_pedido').css('display', 'none');
                $('#container_fecha_habilitar_link_pedido').css('display', 'none');
            } else if ($(this).val()*1 === 2){
                $('#container_cupos').css('display', 'none');
                $('#container_lugar_evento').css('display', 'none');
                $('#container_link').removeAttr('style');
                $('#container_fecha_habilitar_link').removeAttr('style');
                $('#container_link_pedido').removeAttr('style');
                $('#container_fecha_habilitar_link_pedido').removeAttr('style');
            }
        });

        $('#create_event').submit(function(e){
            e.preventDefault();

            let pass = true;


            if($.trim($('#nombre_evento').val()) === ''){ $('#nombre_evento').addClass('my-input-error'); pass = false; }
            else { $('#nombre_evento').removeClass('my-input-error'); }

            if($.trim($('#fecha_evento').val()) === ''){ $('#fecha_evento').addClass('my-input-error'); pass = false; }
            else { $('#fecha_evento').removeClass('my-input-error'); }

            if($.trim($('#hora_evento').val()) === ''){ $('#hora_evento').addClass('my-input-error'); pass = false; }
            else { $('#hora_evento').removeClass('my-input-error'); }

            if($.trim($('#numero_contacto').val()) === ''){ $('#numero_contacto').addClass('my-input-error'); pass = false; }
            else { $('#numero_contacto').removeClass('my-input-error'); }

            if($.trim($('#descripcion').val()) === ''){ $('#descripcion').addClass('my-input-error'); pass = false; }
            else { $('#descripcion').removeClass('my-input-error'); }

            if($('#tipo_evento').val()*1 === 1){
                if($.trim($('#cupos').val()) === ''){ $('#cupos').addClass('my-input-error'); pass = false; }
                else { $('#cupos').removeClass('my-input-error'); }

                if($.trim($('#lugar_evento').val()) === ''){ $('#lugar_evento').addClass('my-input-error'); pass = false; }
                else { $('#lugar_evento').removeClass('my-input-error'); }
            } else {

                if($.trim($('#link').val()) === ''){ $('#link').addClass('my-input-error'); pass = false; }
                else { $('#link').removeClass('my-input-error'); }

                if($.trim($('#fecha_habilitar_link').val()) === ''){ $('#fecha_habilitar_link').addClass('my-input-error'); pass = false; }
                else { $('#fecha_habilitar_link').removeClass('my-input-error'); }

                if($.trim($('#link_pedido').val()) === ''){ $('#link_pedido').addClass('my-input-error'); pass = false; }
                else { $('#link_pedido').removeClass('my-input-error'); }

                if($.trim($('#fecha_habilitar_link_pedido').val()) === ''){ $('#fecha_habilitar_link_pedido').addClass('my-input-error'); pass = false; }
                else { $('#fecha_habilitar_link_pedido').removeClass('my-input-error'); }
            }

            if(pass){

                let form = $('#create_event').serializeArray();
                $.ajax({
                    url: 'createEvent/',
                    data: {"_token": "{{ csrf_token() }}", 'form': form},
                    type: 'POST',
                    dataType: 'json',
                    beforeSend: function () {},
                    success: function (res) {
                        if (typeof res === 'string') res = JSON.parse(res);

                        if(res.Done){
                            $('#submit_button').addClass('disabled');

                            $('#container_message').html(
                                '<div class="alert alert-success alert-dismissible fade in" role="alert">' +
                                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>' +
                                    '</button>' +
                                    '<strong>Exito!</strong> ' + res.Message +
                                '</div>'
                            );

                            setTimeout(function(){ window.location.href = window.location.origin + '/gestion-eventos'; }, 5000);
                        } else {
                            $('#container_message').html(
                                '<div class="alert alert-danger alert-dismissible fade in" role="alert">' +
                                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>' +
                                    '</button>' +
                                    '<strong>Error!</strong> ' + res.Message +
                                '</div>'
                            );
                        }
                    }
                });
            } else {
                $('#container_message').html(
                    '<div class="alert alert-danger alert-dismissible fade in" role="alert">' +
                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>' +
                        '</button>' +
                        '<strong>Ups!</strong> Verifica los campos.' +
                    '</div>'
                );
            }

        });

        // new PNotify({
        //     title: 'Regular Success',
        //     text: 'That thing that you were trying to do worked!',
        //     type: 'success',
        //     styling: 'bootstrap3'
        // });
    </script>
@endsection

