<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <?php $globalUser = Auth::user(); /*dd($globalUser);*/ ?>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Showroom Natura</title>

        <!-- Fonts -->

        <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('css/owl.transitions.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('css/owl.carousel.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('css/animate.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('css/main.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('css/stylesheet.css')}}" rel="stylesheet" type="text/css">

        <style type="text/css">
            @import url({{asset('fonts/NaturaSans-Regular.ttf')}});

            body {
                font-family: 'NaturaSans-Regular', sans-serif !important;
            }
        </style>

        <script type='text/javascript'  src="{{asset('js/jquery.js')}}"></script>
    </head>
    <body id="page-top" data-spy="scroll" data-target=".navbar-custom">
        <div class="navigation">
            <div class="container" id="access_link">
                <?php if($globalUser == null){ ?>
                    <a class="acceder" href="#about">Acceder</a>
                <?php }elseif ($globalUser != null){ ?>
                    <a class="acceder" href="#about">Cerrar Sesi&oacute;n</a>
                <?php }?>
            </div>
        </div>
        <?php if ($globalUser != null){ ?>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
        <?php }?>


        <section class="hero" id="hero">
            <div class="container">
                <div class="caption">
                    <h1 class="text-uppercase  animated wow fadeInLeft">GRAN</h1>
                    <h1 class="text-uppercase  animated wow fadeInLeft">SHOWROOM DE</h1>
                    <h1 class="text-uppercase  animated wow fadeInLeft">NAVIDAD 2019</h1>
                </div>			
            </div>
        </section>

        <header>
            <div class="container">
            
                <nav style="width:1400px">
                    <div class="col-sm-12">
                        <div class="col-sm-6 center">
                            <p class="animated wow fadeInLeft" data-wow-delay=".1s"><a href="#step_by_step_section">Paso a Paso</a></p>
                        </div>
                        <div class="col-sm-6 center">
                            <p class="animated wow fadeInLeft" data-wow-delay=".2s"><a href="#contact_section">Contáctanos</a></p>
                        </div>
                    </div>
                </nav>

                <span class="burger_icon">menu</span>
            </div>
        </header>

        @yield('content') 

        <footer class="hero3" id="contact_section">
            <div class="container">
                <div class="row">
                <div class="col-sm-12">
                    <h1>CONTÁCTANOS</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 footer_left">
                    <p>E-mail: snaccolombia@natura.net</p>
                    <p>Centro de Atención Natura (CAN)</p>
                    <p>Bogotá: 3489540</p>
                    <p>Resto del país Línea Gratuita:</p>
                    <p>018000 969010</p>
                </div>
                <div class="col-sm-6">
                    <div class="center">
                        <img class="footer_img img-responsive" src="{{asset('img/logo.png')}}">
                    </div>
                    <div class="center footer_tyc">
                        <a href="{{ asset('/tyc') }}" class="app_link">
                            <p>Términos y Condiciones</p>
                        </a>
                    </div>
                </div> 
            </div>
            <p class="copyright animated wow fadeIn" data-wow-duration="2s">© 2019 <a href="http://creardigital.com" target="_blank"><strong>creardigital.com</strong></a>. All Rights Reserved</p>
            </div>
        </footer>

    <script type='text/javascript'  src="{{asset('js/scrollTo.js')}}"></script>
    <script type='text/javascript'  src="{{asset('js/owl.carousel.min.js')}}"></script>
    <script type='text/javascript'  src="{{asset('js/wow.js')}}"></script>
    <script type='text/javascript'  src="{{asset('js/parallax.js')}}"></script>
    <script type='text/javascript'  src="{{asset('js/nicescroll.js')}}"></script>
    <script type='text/javascript'  src="{{asset('js/main.js')}}"></script>

    <script type="application/javascript"> 

        const globalUser = '{{ $globalUser }}',
            urlLogin = '{{ asset('login') }}',
            urlLogout = '{{ asset('logout') }}';

        if(globalUser*1 == 0){
            $('#access_link').html('<a class="acceder" href="' + urlLogin + '">Acceder</a>');
        } else if(globalUser != null) {
            $('#access_link').html('<a class="acceder" onclick="$(`#logout-form`).submit()">Cerrar Sesión</a>');
        }
    </script>
    </body>
</html>
