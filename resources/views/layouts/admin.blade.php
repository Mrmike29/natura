<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="images/favicon.ico" type="image/ico" />

        <title>Showroom Exclusivo Natura </title>

        <!-- Bootstrap -->
        <link href="{{asset('css/logged-css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
        <!-- Font Awesome -->
        <link href="{{asset('css/logged-css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
        <!-- NProgress -->
        <link href="{{asset('css/logged-css/nprogress.css')}}" rel="stylesheet" type="text/css">
        <!-- iCheck -->
        <link href="{{asset('css/logged-css/green.css')}}" rel="stylesheet" type="text/css">

        <!-- bootstrap-progressbar -->
        <link href="{{asset('css/logged-css/bootstrap-progressbar-3.3.4.min.css')}}" rel="stylesheet" type="text/css">
        <!-- JQVMap -->
        <link href="{{asset('css/logged-css/jqvmap.min.css')}}" rel="stylesheet" type="text/css">
        <!-- bootstrap-daterangepicker -->
        <link href="{{asset('css/logged-css/daterangepicker.css')}}" rel="stylesheet" type="text/css">

        <!-- Custom Theme Style -->
        <link href="{{asset('css/logged-css/custom.min.css')}}" rel="stylesheet" type="text/css">

        <!-- PNotify -->
        <link href="{{asset('css/logged-css/pnotify.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('css/logged-css/pnotify.buttons.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('css/logged-css/pnotify.nonblock.css')}}" rel="stylesheet" type="text/css">

        <!-- jQuery -->
        <script type='text/javascript'  src="{{asset('js/jquery.js')}}"></script>

        <!-- My Css -->
        <style type="text/css">
            .my-input-error {
                border: 1px solid #e64343;
                -moz-animation: .7s 1 shake linear;
                -webkit-animation: .7s 1 myShake linear;
            }

            @keyframes myShake{
                0%,to{
                    -webkit-transform: translateZ(0);
                    transform: translateZ(0);
                }

                10%,30%,50%,70%,90%{
                    -webkit-transform: translate3d(-10px,0,0);
                    transform: translate3d(-10px,0,0)
                }

                20%,40%,60%,80%{
                    -webkit-transform:translate3d(10px,0,0);
                    transform:translate3d(10px,0,0)
                }
            }
        </style>

        <?php $globalUser = Auth::user(); ?>
    </head>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <div class="col-md-3 left_col">
                    <div class="left_col scroll-view">
                        <div class="navbar nav_title" style="border: 0;">
                            <a href="{{ asset('gestion-eventos') }}" class="site_title"><i class="fa fa-paw"></i> <span>Natura!</span></a>
                        </div>

                        <div class="clearfix"></div>

                        <!-- menu profile quick info -->
                        <div class="profile clearfix">
                            <div class="profile_pic">
                                <img src="{{asset('img/user.png')}}" alt="..." class="img-circle profile_img">
                            </div>
                            <div class="profile_info">
                                <span>Bienvenido(a),</span>
                                <h2>{{$globalUser->name}}</h2>
                            </div>
                        </div>
                        <!-- /menu profile quick info -->

                        <br />

                        <!-- sidebar menu -->
                        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                            <div class="menu_section">
                                <h3>General</h3>
                                <ul class="nav side-menu">
                                    <li><a><i class="fa fa-list-alt"></i> Gesti&oacute;n de Eventos <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu">
                                            <li><a href="{{ asset('gestion-eventos') }}">Listar Eventos</a></li>
                                            <li><a href="{{ asset('crear-evento') }}">Crear Evento</a></li>
                                        </ul>
                                    </li>
                                    <li><a><i class="fa fa-group"></i> Gesti&oacute;n de Admins <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu">
                                            <li><a href="{{ asset('gestion-administradores') }}">Listar Administradores</a></li>
                                            <li><a href="{{ asset('crear-administrador') }}">Crear Administrador</a></li>
                                        </ul>
                                    </li>
                                    <li><a><i class="fa fa-institution"></i> Gesti&oacute;n de Gerencias <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu">
                                            <li><a href="{{ asset('gestion-gerencias') }}">Listar Gerencias</a></li>
                                            <li><a href="{{ asset('crear-gerencia') }}">Crear Gerencia</a></li>
                                        </ul>
                                    </li>

                                    <li><a href="{{ asset('cargar-usuarios') }}"><i class="fa fa-file-excel-o"></i> Cargar Usuarios</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- /sidebar menu -->

                        <!-- /menu footer buttons -->
                        <div class="sidebar-footer hidden-small">
                            <a onclick="$(`#logout-form`).submit()" data-toggle="tooltip" data-placement="top" title="Cerrar Sesi&oacute;n" style="width: 100%">
                                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                            </a>
                            <?php if ($globalUser != null){ ?>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                            <?php }?>
                        </div>
                        <!-- /menu footer buttons -->
                    </div>
                </div>

                <!-- top navigation -->
                <div class="top_nav">
                    <div class="nav_menu">
                        <nav>
                            <div class="nav toggle">
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>
                        </nav>
                    </div>
                </div>
                <!-- /top navigation -->
                    @yield('content')

                <!-- footer content -->
                <footer>
                    <div class="pull-right">
                        Natura - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->
            </div>
        </div>

        <!-- Bootstrap -->
        <script type='text/javascript'  src="{{asset('js/logged-js/bootstrap.min.js')}}"></script>
        <!-- FastClick -->
        <script type='text/javascript'  src="{{asset('js/logged-js/fastclick.js')}}"></script>
        <!-- NProgress -->
        <script type='text/javascript'  src="{{asset('js/logged-js/nprogress.js')}}"></script>
        <!-- Chart.js -->
        <script type='text/javascript'  src="{{asset('js/logged-js/Chart.min.js')}}"></script>
        <!-- gauge.js -->
        <script type='text/javascript'  src="{{asset('js/logged-js/gauge.min.js')}}"></script>
        <!-- bootstrap-progressbar -->
        <script type='text/javascript'  src="{{asset('js/logged-js/bootstrap-progressbar.min.js')}}"></script>
        <!-- iCheck -->
        <script type='text/javascript'  src="{{asset('js/logged-js/icheck.min.js')}}"></script>
        <!-- Skycons -->
        <script type='text/javascript'  src="{{asset('js/logged-js/skycons.js')}}"></script>
        <!-- Flot -->
        <script type='text/javascript'  src="{{asset('js/logged-js/jquery.flot.js')}}"></script>
        <script type='text/javascript'  src="{{asset('js/logged-js/jquery.flot.pie.js')}}"></script>
        <script type='text/javascript'  src="{{asset('js/logged-js/jquery.flot.time.js')}}"></script>
        <script type='text/javascript'  src="{{asset('js/logged-js/jquery.flot.stack.js')}}"></script>
        <script type='text/javascript'  src="{{asset('js/logged-js/jquery.flot.resize.js')}}"></script>
        <!-- Flot plugins -->
        <script type='text/javascript'  src="{{asset('js/logged-js/jquery.flot.orderBars.js')}}"></script>
        <script type='text/javascript'  src="{{asset('js/logged-js/jquery.flot.spline.min.js')}}"></script>
        <script type='text/javascript'  src="{{asset('js/logged-js/curvedLines.js')}}"></script>
        <!-- DateJS -->
        <script type='text/javascript'  src="{{asset('js/logged-js/date.js')}}"></script>
        <!-- JQVMap -->
        <script type='text/javascript'  src="{{asset('js/logged-js/jquery.vmap.js')}}"></script>
        <script type='text/javascript'  src="{{asset('js/logged-js/jquery.vmap.world.js')}}"></script>
        <script type='text/javascript'  src="{{asset('js/logged-js/jquery.vmap.sampledata.js')}}"></script>
        <!-- bootstrap-daterangepicker -->
        <script type='text/javascript'  src="{{asset('js/logged-js/moment.min.js')}}"></script>
        <script type='text/javascript'  src="{{asset('js/logged-js/daterangepicker.js')}}"></script>


        <!-- PNotify -->
        <script type='text/javascript'  src="{{asset('js/logged-js/pnotify.js')}}"></script>
        <script type='text/javascript'  src="{{asset('js/logged-js/pnotify.buttons.js')}}"></script>
        <script type='text/javascript'  src="{{asset('js/logged-js/pnotify.nonblock.js')}}"></script>

        <!-- Custom Theme Scripts -->
        <script type='text/javascript'  src="{{asset('js/logged-js/custom.min.js')}}"></script>

        <script type="application/javascript">
            $(document).ready(function () {
                $('.ui-pnotify.dark.ui-pnotify-fade-normal.ui-pnotify-in.ui-pnotify-fade-in.ui-pnotify-move').remove();
            });
        </script>

    </body>
</html>
