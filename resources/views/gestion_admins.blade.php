@extends('layouts.admin')

@section('content')


    <?php $globalUser = Auth::user(); $cont = 1;?>


    <div class="right_col" role="main">
        <!-- top tiles -->
        <div class="row">
            <div class="clearfix"></div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Gesti&oacuten de Administradores</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">

                        <div class="table-responsive">
                            <table class="table table-striped jambo_table bulk_action">
                                <thead>
                                <tr class="headings">

                                    <th class="column-title">#</th>
                                    <th class="column-title">Nombre</th>
                                    <th class="column-title">Cédula</th>
                                    <th class="column-title">Teléfono</th>
                                    <th class="column-title">Email</th>
                                    <th class="column-title no-link last"><span class="nobr">Action</span>
                                    </th>
                                    <th class="bulk-actions" colspan="7">
                                        <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($admins as $admin)
                                    <tr class="even pointer">
                                        <td class=" ">0{{ $cont }}</td>
                                        <td class=" ">{{ $admin->name }}</td>
                                        <td class=" ">{{ $admin->cedula }}</td>
                                        <td class=" ">{{ $admin->telefono }}</td>
                                        <td class=" ">{{ $admin->email }}</td>
                                        <td class=" last"><a href="/editar-admin/{{ $admin->id }}">Editar</a>
                                        </td>
                                    </tr>
                                    <?php $cont++; ?>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <script type="application/javascript">

    </script>

@endsection