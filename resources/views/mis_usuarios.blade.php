@extends('layouts.master')

@section('content')

<style type="text/css">

</style>

<section class="popular_place padding_top">
    <div class="container">
        <a href="{{ asset('cargar-usuarios') }}" style="float: right" class="genric-btn danger circle arrow">
            Cargar usuarios
        </a>
        <h3 class="mb-30">Tus Eventos</h3>
        <div class="progress-table-wrap">
            <div class="progress-table">
                <div class="table-head">
                    <div class="serial">Nombre</div>
                    <div class="serial">Apellidos</div>
                    <div class="percentage">Email</div>
                </div>
                @foreach($users as $user)
                <div class="table-row">
                    <div class="serial">{{ $user->name }}</div>
                    <div class="serial">{{ $user->last_name }}</div>
                    <div class="percentage">{{ $user->email }}</div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>

<script type="application/javascript">
    function solicitarCupo(evento, user) {
        let token = '{{csrf_token()}}';

        $.ajax({
            url: 'solicitar-cupo/' + token + '/' + evento + '/' + user,
            type: 'GET',
            dataType: 'json',
            beforeSend: function () {},
            success: function (res) {
                if (typeof res === 'string') res = JSON.parse(res);

                alert(res.Message);

                if(res.Done){
                    $('#button_' + evento + '_' + user).text('Participando').removeAttr("onclick");
                }
            },
            error: function () {
            }
        });
    }
</script>

@endsection
