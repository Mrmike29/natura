@extends('layouts.master')

@section('content')

<?php $globalUser = Auth::user(); ?>

        <section class="about" id="about">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 animated wow fadeInRight">
                        <div class="features_list">
                            <h1 class="text-uppercase center">Bienvenido Querido Consultor y Consultora Natura a tu Showroom Exclusivo de Navidad</h1>
                            <p>¡La época más hermosa del año inicia antes de lo esperado!</p>

                            <p> Queremos invitarte a que seas el primer Consultor y Consultora en conocer lo que tiene preparado Natura para este gran evento. Inscríbete, asiste y no te pierdas de todas las dinámicas, promociones exclusivas y la mejor estrategia para tu negocio.</p>

                            <p>¡Vive la magia de la</p>

                            <div style="margin-top:-40px" class="center nn">
							    <h1>Navidad Naranja!</h1>
						    </div>

                            <div class="center">
<a href="{{ asset('/tyc') }}" class="app_tyc">Términos y Condiciones</a>
                            </div>
                        </div>					
                    </div>
                </div>
            </div>
        </section>

        <section class="hero2" id="step_by_step_section">
            <div class="container">
                <div class="center" style="margin-top: -30px">
                    <img class="img_border img-responsive stepbystep" src="{{asset('img/step_by_step.jpg')}}">
                </div>			
            </div>
        </section>
        <section class="sub_box">
            <h1 class="center">REGÍSTRATE</h1>
            <p class="cta_text animated wow fadeInDown">Para poder asistir a nuestro Showroom Exclusivo, debes reservar tu lugar ingresando con los siguientes datos para registrarte.</p>
            <form action="{{ route('registrarse') }}" method="POST" class="animated wow fadeIn" data-wow-duration="2s" id="register_form"  enctype="multipart/form-data">
                @csrf
                <p>
                    <select class="mc-input" name="" id="">
                        <option value="CC">CC</option>
                    </select>
                </p>
                <p>
                    <input class="mc-input" type="number" id="cedula" name="cedula" placeholder="Ingresa el número de documento"/>
                </p>
                <p>
                    <input class="mc-input" type="number" id="codigo" name="codigo" placeholder="Ingresa tu código de persona"/>
                    <input type="hidden" id="u_i" name="u_i" value=""/>
                </p>
                <button class="send_button" type="submit" id="mc_submit" placeholder="Ingresa tu código de persona"> <p>Ingresar</p> </button>
            </form>
        </section>


    <script type="application/javascript">
        $('#register_form').submit(function(event) {
            event.preventDefault();
            let token = '{{csrf_token()}}',
                pass = true;

            if($.trim($('#cedula').val()) === ''){ pass = false; }
            if($.trim($('#codigo').val()) === ''){ pass = false; }

            if(pass){
                $.ajax({
                    url: 'validate-user-exist/' + $.trim($("#cedula").val()) + '/' + $.trim($("#codigo").val()) + '/' + token,
                    type: 'GET',
                    dataType: 'json',
                    beforeSend: function () {},
                    success: function (res) {
                        if (typeof res === 'string') res = JSON.parse(res);

                        if(res.Done){
                            $('#u_i').val(res.Data); 
                            $('#register_form').unbind('submit').submit();
                        } else {
                            alert(res.Message);
                        }
                    }
                });
            }
            else{ alert("Por favor, verifique los campos."); }
        })
    </script>

    <script type="application/javascript">

        function enviarCorreo() {
            $.ajax({
                url: '/enviar-correo',
                type: 'GET',
                dataType: 'json',
                beforeSend: function () {
                },
                success: function (res) {
                    if (typeof res === 'string') res = JSON.parse(res);

                    alert(res.Message);
                }
            });
        }

        

        function solicitarCupo() {

        

            let token = '{{csrf_token()}}',
                data = '',
                pass = true,
                step = $('#solicitar_cupo').data('step');

            if(step*1 === 1){
                if ($.trim($("#cedula").val()) === ''){ pass = false; }
                if ($.trim($("#codigo").val()) === ''){ pass = false; }

                data = 'cedula=' + $.trim($("#cedula").val()) + '&' + 'codigo=' + $.trim($("#codigo").val());
            } else if(step*1 === 2) {
                if ($.trim($("#correo").val()) === ''){ pass = false; }
                if ($.trim($("#telefono").val()) === ''){ pass = false; }

                data = 'correo=' + $.trim($("#correo").val()) +
                    '&' + 'telefono=' + $.trim($("#telefono").val()) +
                    '&' + 'divulgacion=' + $("#divulgacion").prop("checked") +
                    '&' + 'i_u=' + $("#i_u").val() +
                    '&' + 'i_g=' + $("#i_g").val();
            } else if (step*1 === 3) {

                if($('[name="evento"]:checked').val() == undefined){ pass = false; }
                data = 'evento=' + $('[name="evento"]:checked').val() + '&' + 'i_u=' + $("#i_u").val();
            }

            if(pass){
                $.ajax({
                url: 'solicitar-cupo/' + step + '/' + data + '/' + token,
                type: 'GET',
                dataType: 'json',
                beforeSend: function () {},
                success: function (res) {
                    if (typeof res === 'string') res = JSON.parse(res);

                    if(res.Done){
                        if(res.Data.boolLastStep){
                            $('#content_showroom').html(
                                '<h1 class="ui header" style="color: #ac7c4f;">REG&Iacute;STRATE</h1>' +
                                '<h2 class="ui header" style="color: #ac7c4f; margin-bottom: 45px">' +
                                    ((res.Data.dataEvent.id_tipo_evento*1 === 1)?
                                        'Quedaste registrado para asistir al Showroom exclusivo de Navidad, Tu confirmación ha sido enviada a tu correo electrónico' :
                                        'Quedaste registrado para asistir al Showroom exclusivo de Navidad Virtual') +
                                '</h2>' +
                                '<p  style="font-size: 20px; color: #757c78; margin-bottom: 0;">' +
                                    '<strong>' +
                                        '<span style="color: #f60;">FECHA: </span>' +
                                    '</strong>' +
                                    res.Data.dataEvent.fecha_realizar + '<br><br>' +

                                    '<strong>' +
                                        '<span style="color: #f60;">HORA: </span>' +
                                    '</strong> ' +
                                    res.Data.dataEvent.hora_realizar + '<br><br>' +

                                    ((res.Data.dataEvent.id_tipo_evento*1 === 1)?
                                    '<strong>' +
                                        '<span style="color: #f60;">LUGAR: </span>' +
                                    '</strong> '
                                        + res.Data.dataEvent.lugar + '' :
                                    '') + '<br><br>' +

                                    res.Data.dataEvent.descripcion + '<br><br>' +

                                    'Si tienes alguna inquietud puedes comunicarte con nosotros al CAN al número ' +
                                    res.Data.dataEvent.numero_contacto +
                                '</p>'
                            );
                        }else if(step*1 === 1){
                            $('#content_showroom').html(
                                '<h1 class="ui header" style="color: #ac7c4f;">REG&Iacute;STRATE</h1>' +
                                '<h2 class="ui header" style="color: #ac7c4f; margin-bottom: 45px">' +
                                    'Bienveido(a) ' + res.Data.boolUser.uc_nombre +
                                '</h2>' +
                                '<p  style="font-size: 20px; color: #757c78; margin-bottom: 0;">' +
                                    '<strong>' +
                                        '<span style="color: #f60;">NOMBRE: </span>' +
                                    '</strong>' +
                                    res.Data.boolUser.uc_nombre + '<br>' +

                                    '<strong>' +
                                        '<span style="color: #f60;">GERENCIA: </span>' +
                                    '</strong>' +
                                    res.Data.boolUser.g_nombre + '<br>' +

                                    '<strong>' +
                                        '<span style="color: #f60;">SECTOR: </span>' +
                                    '</strong>' +
                                    res.Data.boolUser.s_nombre + '<br>' +

                                    '<strong>' +
                                        '<span style="color: #f60;">GDN: </span>' +
                                    '</strong>' +
                                        res.Data.boolUser.name + '<br><br>' +
                                '</p>' +
                                '<form class="ui form" id="register_form">' +
                                    '<div class="field">' +
                                        '<input type="hidden" id="i_u" value="' + res.Data.boolUser.uc_id + '">' +
                                        '<input type="hidden" id="i_g" value="' + res.Data.boolUser.g_id + '">' +
                                        '<input type="email" id="correo" name="correo" placeholder="Correo Electrónico">' +
                                    '</div>' +
                                    '<div class="field">' +
                                        '<input type="number" id="telefono" name="telefono" placeholder="Teléfono">' +
                                    '</div>' +
                                    '<div class="field">' +
                                        '<div class="ui checkbox">' +
                                            '<input type="checkbox" id="divulgacion" name="divulgacion" tabindex="0" class="">' +
                                            '<label>Acepto divulgación de datos</label>' +
                                        '</div>' +
                                    '</div>' +
                                    '<button class="ui button continue-botton" onClick="solicitarCupo()" id="solicitar_cupo" data-step="2">Continuar</button>' +
                                '</form>'
                            );
                            $('#divulgacion').change(function () {
                                if(!$(this).prop("checked")){
                                    $('#solicitar_cupo').attr("disabled", true);
                                    $('#solicitar_cupo').attr("readonly", true);
                                    $('#solicitar_cupo').css("display", "none");
                                } else {
                                    $('#solicitar_cupo').attr("disabled", false);
                                    $('#solicitar_cupo').attr("readonly", false);
                                    $('#solicitar_cupo').removeAttr('style');
                                }
                            });
                            $('#divulgacion').change();
                        } else if(step*1 === 2) {
                            $('#content_showroom').html(
                                '<h1 class="ui header" style="color: #ac7c4f;">REG&Iacute;STRATE</h1>' +
                                '<h2 class="ui header" style="color: #ac7c4f; margin-bottom: 45px">' +
                                    'Tienes disponible estos eventos de showroom exclusivo de navidad anticipado, por favor escoge una opción' +
                                '</h2>' +
                                '<form class="ui form" id="register_form">' +
                                ((res.Data.presencial)?
                                    '<p style="font-size: 20px; color: #757c78; margin-bottom: 0;">' +
                                        '<strong>' +
                                            '<span style="color: #f60;">FECHA: </span>' +
                                        '</strong>' +
                                        res.Data.getPresentialEvent.fecha_realizar + '<br>' +

                                        '<strong>' +
                                            '<span style="color: #f60;">HORA: </span>' +
                                        '</strong>' +
                                        res.Data.getPresentialEvent.hora_realizar + '<br>' +

                                        '<strong>' +
                                            '<span style="color: #f60;">LUGAR: </span>' +
                                        '</strong>' +
                                        res.Data.getPresentialEvent.lugar + '<br>' +
                                    '</p>' +
                                    '<br>' +
                                    '<div class="field">' +
                                        '<div class="ui radio checkbox">' +
                                            '<input type="radio" id="evento" name="evento" value="' + res.Data.getPresentialEvent.id + '">' +
                                            '<label>Asistir</label>' +
                                        '</div>' +
                                    '</div>' +
                                    '<p style="font-size: 20px; color: #757c78; margin-bottom: 0;">' + res.Data.getPresentialEvent.descripcion + '</p>' : '') +

                                ((res.Data.virtual)?
                                    '<p style="font-size: 20px; color: #757c78; margin-bottom: 0;">' +
                                        '<strong>' +
                                            '<span style="color: #f60;">FECHA: </span>' +
                                        '</strong>' +
                                        res.Data.getPresentialEvent.fecha_realizar + '<br>' +

                                        '<strong>' +
                                            '<span style="color: #f60;">HORA: </span>' +
                                        '</strong>' +
                                        res.Data.getPresentialEvent.hora_realizar + '<br>' +
                                    '</p>' +
                                    '<br>' +
                                    '<div class="field">' +
                                        '<div class="ui radio checkbox">' +
                                            '<input type="radio" id="evento" name="evento" value="' + res.Data.getPresentialEvent.id + '">' +
                                            '<label>Asistir</label>' +
                                        '</div>' +
                                    '</div>' +
                                    '<p style="font-size: 20px; color: #757c78; margin-bottom: 0;">' + res.Data.getPresentialEvent.descripcion + '</p>' : '') +

                                    '<button class="ui button continue-botton" onClick="solicitarCupo()" id="solicitar_cupo" data-step="3">Continuar</button>' +
                                '</form>'
                            );

                            $('[name="evento"]').change(function () {
                                if($('[name="evento"]:checked').val() == undefined){
                                    $('#solicitar_cupo').attr("disabled", true);
                                    $('#solicitar_cupo').attr("readonly", true);
                                    $('#solicitar_cupo').css("display", "none");
                                } else {
                                    $('#solicitar_cupo').attr("disabled", false);
                                    $('#solicitar_cupo').attr("readonly", false);
                                    $('#solicitar_cupo').removeAttr('style');
                                }
                            });
                            $('[name="evento"]').change();
                        }
                    } else {
                        alert(res.Message);
                    }
                },
                error: function () {
                }
                });
            } else {
                alert("Debe llenar todos los campos antes de continuar");
            }
        }
    </script>

@endsection
