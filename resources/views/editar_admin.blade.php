@extends('layouts.admin')

@section('content')

    <?php $globalUser = Auth::user(); ?>

    <div class="right_col" role="main">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Editar Administrador</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div id="container_message">

                        </div>
                        <br />
                        <form class="form-horizontal form-label-left" id="edit_admin_form" method="POST" action="{{ route('editAdmin') }}" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nombre<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input class="form-control col-md-7 col-xs-12" type="text" name="nombre" id="nombre" placeholder="Nombre" value="{{ $user->name }}">
                                    <input type="hidden" name="id" id="id" value="{{ $user->id }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Correo Electrónico<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input class="form-control col-md-7 col-xs-12" type="email" name="email" id="email" placeholder="Correo Electrónico" value="{{ $user->email }}">
                                </div>
                            </div>
                            <div class="form-group" >
                                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Cédula<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input class="form-control col-md-7 col-xs-12" type="number" name="cedula" id="cedula" placeholder="Cédula" value="{{ $user->cedula }}">
                                </div>
                            </div>

                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-primary" id="submit_button">Guardar</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="application/javascript">
        $('#edit_admin_form').submit(function(event) {
            event.preventDefault();
            let pass = true;


            if($.trim($('#nombre').val()) === ''){ $('#nombre').addClass('my-input-error'); pass = false; }
            else { $('#nombre').removeClass('my-input-error'); }

            if($.trim($('#email').val()) === ''){ $('#email').addClass('my-input-error'); pass = false; }
            else { $('#email').removeClass('my-input-error'); }

            if($.trim($('#cedula').val()) === ''){ $('#cedula').addClass('my-input-error'); pass = false; }
            else { $('#cedula').removeClass('my-input-error'); }

            if(pass){

                let form = $('#edit_admin_form').serializeArray();

                $.ajax({
                    url: '/editAdmin/',
                    data: {"_token": "{{ csrf_token() }}", 'form': form},
                    type: 'POST',
                    dataType: 'json',
                    beforeSend: function () {},
                    success: function (res) {
                        if (typeof res === 'string') res = JSON.parse(res);

                        if(res.Done){
                            $('#submit_button').addClass('disabled');

                            $('#container_message').html(
                                '<div class="alert alert-success alert-dismissible fade in" role="alert">' +
                                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>' +
                                    '</button>' +
                                    '<strong>Exito!</strong> ' + res.Message +
                                '</div>'
                            );

                            setTimeout(function(){ location.reload(); }, 5000);
                        } else {
                            $('#container_message').html(
                                '<div class="alert alert-danger alert-dismissible fade in" role="alert">' +
                                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>' +
                                    '</button>' +
                                    '<strong>Error!</strong> ' + res.Message +
                                '</div>'
                            );
                        }
                    }
                });
            } else {
                $('#container_message').html(
                    '<div class="alert alert-danger alert-dismissible fade in" role="alert">' +
                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>' +
                        '</button>' +
                        '<strong>Ups!</strong> Verifica los campos.' +
                    '</div>'
                );
            }
        })
    </script>
@endsection

