<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<?php $globalUser = Auth::user(); /*dd($globalUser);*/ ?>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Showroom Natura</title>

        <!-- CSS -->

        <!-- Bootstrap -->
        <link href="{{asset('css/logged-css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
        <!-- Font Awesome -->
        <link href="{{asset('css/logged-css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
        <!-- NProgress -->
        <link href="{{asset('css/logged-css/nprogress.css')}}" rel="stylesheet" type="text/css">
        <!-- Animate.css -->
        <link href="{{asset('css/logged-css/animate.min.css')}}" rel="stylesheet" type="text/css">
        <!-- Custom Theme Style -->
        <link href="{{asset('css/logged-css/custom.min.css')}}" rel="stylesheet" type="text/css">


        <style type="text/css">
            @import url({{asset('fonts/NaturaSans-Regular.ttf')}});

            body {
                font-family: 'NaturaSans-Regular', sans-serif !important;
            }
        </style>

        <script type='text/javascript'  src="{{asset('js/jquery.js')}}"></script>
    </head>
    <body class="login">
        <div>
            <a class="hiddenanchor" id="signup"></a>
            <a class="hiddenanchor" id="signin"></a>

            <div class="login_wrapper">
                <div class="animate form login_form">
                    <section class="login_content">
                        <form method="POST" action="{{ route('login') }}">
                        @csrf
                            <h1>INICIAR SESIÓN</h1>
                            <h5>Login exclusivo para usuarios administradores y gerentes
                                Ingrese los datos solicitados a continución:</h5>
                            <div>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Email">
                            </div>
                            <div>
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Contraseña">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ 'Las credenciales ingresadas no coinciden con nuestros datos.' }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div>
                                <button type="submit" class="btn btn-default submit">Ingresar</button>
                            </div>

                            <div class="clearfix"></div>

                            <div class="separator">

                                <div class="clearfix"></div>
                                <br />

                                <div>
                                    <h1><i class="fa fa-paw"></i> Natura</h1>
                                    <p>©2016 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>
                                </div>
                            </div>
                        </form>
                    </section>
                </div>
            </div>
        </div>
    </body>
</html>

