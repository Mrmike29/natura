@extends('layouts.admin')

@section('content')

    <?php $globalUser = Auth::user(); ?>


    <style type="text/css">
        .continue-botton {
            width: 60%;
            height: 50px;
            border-radius: 15px !important;
            color: #fff !important;
            background: #ac7c4f !important;
            font-size: 1.5em !important;
        }
    </style>

    <div class="ui center aligned container">
        <div class="ui very relaxed items">
            <?php if($globalUser['id_role'] === 1){ ?>
                <div class="item">
                    <div class="middle aligned content">
                        <a class="ui button continue-botton" href="{{'/gestion-administradores'}}">Gestión de Administradores</a>
                    </div>
                </div>
                <div class="item">
                    <div class="middle aligned content">
                        <a class="ui button continue-botton" href="{{'/gestion-gerencias'}}">Gestión de Gerencias</a>
                    </div>
                </div>
            <?php } ?>
            <div class="item">
                <div class="middle aligned content">
                    <a class="ui button continue-botton" href="{{'/gestion-eventos'}}">Gestión de Eventos</a>
                </div>
            </div>
            <div class="item">
                <div class="middle aligned content">
                    <a class="ui button continue-botton" href="{{'/cargar-usuarios'}}">Cargar Usuarios</a>
                </div>
            </div>
        </div>
    </div>

@endsection
