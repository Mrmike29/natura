@extends('layouts.master')

@section('content')

    <?php $globalUser = Auth::user(); ?>

    <style type="text/css">
        .continue-botton {
            width: 60%;
            height: 50px;
            border-radius: 15px !important;
            color: #fff !important;
            background: #ac7c4f !important;
            font-size: 1.5em !important;
            padding: 0 !important;
        }
    </style>

    <section class="sub_box">
        <div id="content_showroom">
        <h1 class="center">REGÍSTRATE</h1>
        <p style="color:wheat;" class="cta_text animated wow fadeInDown">Para poder asistir a nuestro Showroom Exclusivo, debes reservar tu lugar ingresando con los siguientes datos para registrarte.</p>
        
            
        </div>
        <input type="hidden" name="id" id="id" value="{{ $id }}">
        <input type="hidden" name="step" id="step" value="{{ $step }}">
        <input type="hidden" name="registered" id="registered" value="{{ ($registered)? 1 : 0 }}"> 
    </section>


    <script type="application/javascript">
        function solicitarCupo() {
            let token = '{{csrf_token()}}',
                data = '',
                pass = true,
                step = $('#step').val()*1;

            console.log(step);

            if(step === 1){
                data = 'id=' + $.trim($("#id").val()) + '&' + 'registered=' + $("#registered").val();;
            } else if(step === 2) {
                if ($.trim($("#correo").val()) === ''){ pass = false; }
                if ($.trim($("#telefono").val()) === ''){ pass = false; }

                data = 'correo=' + $.trim($("#correo").val()) +
                    '&' + 'telefono=' + $.trim($("#telefono").val()) +
                    '&' + 'divulgacion=' + $("#divulgacion").prop("checked") +
                    '&' + 'i_u=' + $("#i_u").val() +
                    '&' + 'i_g=' + $("#i_g").val();
            } else if (step*1 === 3 && $('#registered').val()*1 === 0) {

                if($('[name="evento"]:checked').val() == undefined){ pass = false; }
                data = 'evento=' + $('[name="evento"]:checked').val() + '&' + 'i_u=' + $("#i_u").val();
            }

            if(pass){  
                $.ajax({
                    url: 'solicitar-cupo/' + step + '/' + data + '/' + token,
                    type: 'GET',
                    dataType: 'json',
                    beforeSend: function () {},
                    success: function (res) {
                        if (typeof res === 'string') res = JSON.parse(res);

                        if(res.Done){
                            if(res.Data.boolLastStep){
                                $('#content_showroom').html(
                                    '<h1 class="ui header" style="color: #ac7c4f;">REG&Iacute;STRATE</h1>' +
                                    '<p style="color:wheat;" class="cta_text animated wow fadeInDown">' +
                                        ((res.Data.dataEvent.id_tipo_evento*1 === 1)?
                                            'Quedaste registrado para asistir al Showroom exclusivo de Navidad, Tu confirmación ha sido enviada a tu correo electrónico' :
                                            'Quedaste registrado para asistir al Showroom exclusivo de Navidad Virtual') +
                                    '</p>' +
                                    '<p  style="font-size: 20px; color: #757c78; margin-bottom: 0;">' +
                                        '<strong>' +
                                            '<span style="color: #f60;">FECHA: </span>' +
                                        '</strong>' +
                                        res.Data.dataEvent.fecha_realizar + '<br><br>' +

                                        '<strong>' +
                                            '<span style="color: #f60;">HORA: </span>' +
                                        '</strong> ' +
                                        res.Data.dataEvent.hora_realizar + '<br><br>' +

                                        ((res.Data.dataEvent.id_tipo_evento*1 === 1)?
                                        '<strong>' +
                                            '<span style="color: #f60;">LUGAR: </span>' +
                                        '</strong> '
                                            + res.Data.dataEvent.lugar + '' :
                                        '') + '<br><br>' +

                                        res.Data.dataEvent.descripcion + '<br><br>' +

                                        'Si tienes alguna inquietud puedes comunicarte con nosotros al CAN al número ' +
                                        res.Data.dataEvent.numero_contacto +
                                    '</p>'
                                );
                            } else if(step === 1){
                                $('#content_showroom').html(
                                    '<h1 class="center">REGÍSTRATE</h1>' +
                                    '<p style="color:wheat;" class="cta_text animated wow fadeInDown">' +
                                        'Bienveido(a) ' + res.Data.boolUser.uc_nombre +
                                    '</p>' +
                                    '<p  style="font-size: 20px; color: #757c78; margin-bottom: 0;">' +
                                        '<strong>' +
                                            '<span style="color: #f60;">NOMBRE: </span>' +
                                        '</strong>' +
                                        res.Data.boolUser.uc_nombre + '<br>' +

                                        '<strong>' +
                                            '<span style="color: #f60;">GERENCIA: </span>' +
                                        '</strong>' +
                                        res.Data.boolUser.g_nombre + '<br>' +

                                        '<strong>' +
                                            '<span style="color: #f60;">SECTOR: </span>' +
                                        '</strong>' +
                                        res.Data.boolUser.s_nombre + '<br>' +

                                        '<strong>' +
                                            '<span style="color: #f60;">GDN: </span>' +
                                        '</strong>' +
                                            res.Data.boolUser.name + '<br><br>' +
                                    '</p>' +
                                    '<p>' +
                                        '<input type="hidden" id="i_u" value="' + res.Data.boolUser.uc_id + '">' +
                                        '<input type="hidden" id="i_g" value="' + res.Data.boolUser.g_id + '">' +
                                        '<input type="email" id="correo" name="correo" placeholder="Correo Electrónico">' +
                                    '</p>' +
                                    '<p>' +
                                        '<input type="number" id="telefono" name="telefono" placeholder="Teléfono">' +
                                    '</p>' +
                                    '<p>' +
                                        '<div class="ui checkbox">' +
                                            '<input type="checkbox" id="divulgacion" name="divulgacion" tabindex="0" class="">' +
                                            '<label>Acepto divulgación de datos</label>' +
                                        '</div>' +
                                    '</p>' +
                                    '<button id="mc_submit" type="button" onClick="solicitarCupo()">Continuar</button>'
                                );

                                $('#step').val(2);

                                $('#divulgacion').change(function () {
                                    if(!$(this).prop("checked")){
                                        $('#mc_submit').attr("disabled", true);
                                        $('#mc_submit').attr("readonly", true);
                                        $('#mc_submit').css("display", "none");
                                    } else {
                                        $('#mc_submit').attr("disabled", false);
                                        $('#mc_submit').attr("readonly", false);
                                        $('#mc_submit').removeAttr('style');
                                    }
                                });
                                $('#divulgacion').change();
                            } else if(step*1 === 2) {
                                $('#content_showroom').html(
                                    '<h1 class="center">REGÍSTRATE</h1>' +
                                    '<p style="color:wheat;" class="cta_text animated wow fadeInDown">' +
                                        'Tienes disponible estos eventos de showroom exclusivo de navidad anticipado, por favor escoge una opción' +
                                    '</p>' +

                                    ((res.Data.presencial)?
                                        '<p style="font-size: 20px; color: #757c78; margin-bottom: 0;">' +
                                            '<strong>' +
                                                '<span style="color: #f60;">FECHA: </span>' +
                                            '</strong>' +
                                            res.Data.getPresentialEvent.fecha_realizar + '<br>' +

                                            '<strong>' +
                                                '<span style="color: #f60;">HORA: </span>' +
                                            '</strong>' +
                                            res.Data.getPresentialEvent.hora_realizar + '<br>' +

                                            '<strong>' +
                                                '<span style="color: #f60;">LUGAR: </span>' +
                                            '</strong>' +
                                            res.Data.getPresentialEvent.lugar + '<br>' +
                                        '</p>' +
                                        '<br>' +
                                        '<p>' +
                                            '<div class="ui radio checkbox">' +
                                                '<input type="radio" id="evento" name="evento" value="' + res.Data.getPresentialEvent.id + '">' +
                                                '<label>Asistir</label>' +
                                            '</div>' +
                                        '</p>' +
                                        '<p style="font-size: 20px; color: #757c78; margin-bottom: 0;">' + res.Data.getPresentialEvent.descripcion + '</p>' : '') +

                                    ((res.Data.virtual)?
                                        '<p style="font-size: 20px; color: #757c78; margin-bottom: 0;">' +
                                            '<strong>' +
                                                '<span style="color: #f60;">FECHA: </span>' +
                                            '</strong>' +
                                            res.Data.getPresentialEvent.fecha_realizar + '<br>' +

                                            '<strong>' +
                                                '<span style="color: #f60;">HORA: </span>' +
                                            '</strong>' +
                                            res.Data.getPresentialEvent.hora_realizar + '<br>' +
                                        '</p>' +
                                        '<br>' +
                                        '<p>' +
                                            '<div class="ui radio checkbox">' +
                                                '<input type="radio" id="evento" name="evento" value="' + res.Data.getPresentialEvent.id + '">' +
                                                '<label>Asistir</label>' +
                                            '</div>' +
                                        '</p>' +
                                        '<p style="font-size: 20px; color: #757c78; margin-bottom: 0;">' + res.Data.getPresentialEvent.descripcion + '</p>' : '') +
                                
                                    '<button id="mc_submit" type="button" onClick="solicitarCupo()">Continuar</button>'
                                );
                                $('#step').val(3);

                                $('[name="evento"]').change(function () {
                                    if($('[name="evento"]:checked').val() == undefined){
                                        $('#mc_submit').attr("disabled", true);
                                        $('#mc_submit').attr("readonly", true);
                                        $('#mc_submit').css("display", "none");
                                    } else {
                                        $('#mc_submit').attr("disabled", false);
                                        $('#mc_submit').attr("readonly", false);
                                        $('#mc_submit').removeAttr('style');
                                    }
                                });
                                $('[name="evento"]').change();
                            }
                        } else {
                            alert(res.Message);
                        }
                    }
                    
                });
            } else {
                alert("Debe llenar todos los campos antes de continuar");
            }
        }

        solicitarCupo();
    </script>
@endsection

