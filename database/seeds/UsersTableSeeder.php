<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Mike',
                'email' => 'maikmagic@hotmail.com',
                'password' => bcrypt('password')
            ],
            [
                'name' => 'Mike2',
                'email' => 'maikmagic2@hotmail.com',
                'password' => bcrypt('password')
            ]
        ]);
    }
}
