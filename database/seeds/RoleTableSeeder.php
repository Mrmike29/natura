<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                'name' => 'Admin',
                'slug' => 'admin',
                'description' => 'Puede hacer de todo'
            ],
            [
                'name' => 'Deny',
                'slug' => 'deny',
                'description' => 'no puede hacer nada'
            ],
            [
                'name' => 'Usuarios',
                'slug' => 'usuarios',
                'description' => 'Usuario estandar'
            ]
        ]);
    }
}
