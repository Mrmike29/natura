<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsuariosComunes extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'usuarios_comunes';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cod_persona',
	    'cedula',
	    'nombre',
	    'nivel',
	    'cod_gerencia',
	    'gerencia',
	    'cod_sector',
	    'sector',
		'gdn',
		'cod_grupo',
		'ciclo',
		'perfil_activo',
		'id_gerencia',
        'evento_presencial',
        'evento_virtual'
    ];
}
