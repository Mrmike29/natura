<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imports\UsersImport;
use App\Exports\UsersListExport;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MainController extends Controller
{

    public function getRoute($route = null){

        $view = view('error');

        if($route === 'home' || $route === null){

            $view = view('home', ['ruta' => $route]);

        } elseif($route === 'login'){

            $view = view('auth/login');

        } elseif($route === 'registro'){

            $view = view('auth/register');

        }

        return view('master', ['view' => $view]);
    }

    public function index()
    {

        if(Auth::user() != null){
            return redirect('/gestion-eventos');
        }

        return view('home');
    }

    public function getGestion(){ return redirect('/gestion-eventos'); }

    public function eventos(){

        if(Auth::user() == null){ return abort(403, 'Accion no autorizada');}
        if(Auth::user()->id_role != 1 && Auth::user()->id_role != 2){ return abort(403, 'Accion no autorizada'); }

        $id = Auth::user()->id;

        if(Auth::user()['id_role'] == 1){
            $events = DB::table('eventos')
                ->select('eventos.*')
                ->get();
        }elseif(Auth::user()['id_role'] == 2){
            $events = DB::table('eventos')
                ->select('eventos.*')
                ->join('gerencias AS G', 'G.id', 'id_gerencia')
                ->join('users AS U', 'U.id', 'G.id_user')
                ->where('U.id', $id)
                ->get();
        }elseif(Auth::user()['id_role'] == 3) {
            $events = DB::table('eventos')
                ->select('eventos.id', 'eventos.nombre', 'eventos.descripcion', 'eventos.fecha_realizar', 'UE.id_user AS id_user', 'UE.id AS UE_id')
                ->join('users_eventos AS UE', 'UE.id_evento', 'eventos.id')
                ->where('UE.id_user', $id)
                ->get();
        }

        return view('eventos', ['events' => $events]);
    }

    public function crearEvento($edit = false, $insert = false){

        if(Auth::user() == null){ return abort(403, 'Accion no autorizada');}
        if(Auth::user()->id_role != 1 && Auth::user()->id_role != 2){ return abort(403, 'Accion no autorizada'); }

        $id = Auth::user()->id;

        $idGerencia = null;

        if($idGerencia === null && Auth::user()['id_role'] == 2){
            $gerencia = DB::table('gerencias')
                ->select('id')
                ->where('id_user', $id)
                ->first();

            $idGerencia = $gerencia->id;
        } else {
            $idGerencia = DB::table('gerencias')
                ->select('*')
                ->get();
        }

        $events = DB::table('eventos')
            ->select('eventos.*')
            ->join('gerencias AS G', 'G.id', 'id_gerencia')
            ->join('users AS U', 'U.id', 'G.id_user')
            ->where('U.id', $id)
            ->get();

        if($edit){
            if($insert){
                echo "<script>alert('Evento creado con éxito');</script>";
            } else {
                echo "<script>alert('No se pudieron insertar los datos');</script>";
            }
        }

        return view('crear_evento', ['idGerencia' => $idGerencia]);
    }

    public function createEvent(Request $request){

        if(Auth::user() == null){ return abort(403, 'Accion no autorizada');}
        if(Auth::user()->id_role != 1 && Auth::user()->id_role != 2){ return abort(403, 'Accion no autorizada'); }

        $id = Auth::user()->id;
        $idGerencia = $request->form[1]['value'];
        $tipoEvento = $request->form[2]['value'];
        $nombreEvento = $request->form[3]['value'];
        $fechaEvento = $request->form[4]['value'];
        $horaEvento = $request->form[5]['value'];
        $cupos = $request->form[6]['value'];
        $numeroContacto = $request->form[7]['value'];
        $link = $request->form[8]['value'];
        $fechaHabilitarLink = $request->form[9]['value'];
        $linkPedido = $request->form[10]['value'];
        $fechaHabilitarLinkPedido = $request->form[11]['value'];
        $lugarEvento = $request->form[12]['value'];
        $desc = $request->form[13]['value'];


        $insert = DB::table('eventos')
            ->insert([
                'id_gerencia' => $idGerencia,
                'id_tipo_evento' =>$tipoEvento,
                'nombre' => $nombreEvento,
                'lugar' => $lugarEvento,
                'descripcion' => $desc,
                'cupos' => $cupos,
                'fecha_realizar' => $fechaEvento,
                'hora_realizar' => $horaEvento,
                'numero_contacto' => $numeroContacto,
                'link_evento' => $link,
                'fecha_habilitar_link' => $fechaHabilitarLink,
                'link_ordenador' => $linkPedido,
                'fecha_habilitar_link_ordenador' => $fechaHabilitarLinkPedido
                ]
            );

        if($insert){
            return response()->json([
                'Done' => true,
                'Status' => 'Success',
                'Message' => 'El evento fue creado exitosamente.',
                'Data' => null,
                'Header' => 200
            ]);
        }else {
            return response()->json([
                'Done' => false,
                'Status' => 'Error',
                'Message' => 'No se pudo crear el evento.',
                'Data' => null,
                'Header' => 202
            ]);
        }
    }

    public function misUsuarios(){

        $permisos = auth()->user()->permissions()->pluck('slug')->toArray();

        if(!in_array('home', $permisos)){
            return abort(403, 'Accion no autorizada');
        }

        Auth::routes();

        $id = Auth::user()->id;

        $gerencia = DB::table('gerencias')
            ->select('id')
            ->where('id_user', $id)
            ->first();

        $users = DB::table('users AS U')
            ->select('U.*')
            ->join('users_gerencias AS UG', 'UG.id_user', 'U.id')
            ->where('UG.id_gerencia', $gerencia->id)
            ->get();

        return view('mis_usuarios', ['users' => $users]);
    }

    public function cargarUsuarios(){

        if(Auth::user() == null){ return abort(403, 'Accion no autorizada');}
        if( Auth::user()->id_role != 1 && Auth::user()->id_role != 2){ return abort(403, 'Accion no autorizada'); }
        return view('cargar_usuarios');
    }

    public function upload(){

        if(Auth::user() == null){ return abort(403, 'Accion no autorizada');}
        if(Auth::user()->id_role != 1 && Auth::user()->id_role != 2){ return abort(403, 'Accion no autorizada'); }

        Excel::import(new UsersImport, request()->file('excel'));
        return redirect('/cargar-usuarios')->with('success', 'All good!');
    }

    public function donwloadEventUsers($id){

        if(Auth::user() == null){ return abort(403, 'Accion no autorizada');}
        if(Auth::user()!= null && Auth::user()->id_role != 1 && Auth::user()->id_role != 2){ return abort(403, 'Accion no autorizada'); }

        return  Excel::download(new UsersListExport($id), 'listado.xlsx');

    }

    public function getGestionAdministradores(){

        if(Auth::user()->id_role != 1){
            return abort(403, 'Accion no autorizada');
        }

        $admins = DB::table('users')
            ->select('*')
            ->where('id_role', 1)
            ->get();

        return view('gestion_admins', ['admins' => $admins]);
    }

    public function getGestionGerencias(){

        if(Auth::user()->id_role != 1){
            return abort(403, 'Accion no autorizada');
        }

        Auth::routes();

        $id = Auth::user()->id;

        $gerencias = DB::table('gerencias')
            ->select('gerencias.*', 'U.name', 'U.email', 'S.nombre AS sector_name', 'S.cod AS sector_cod')
            ->join('users AS U', 'U.id', 'id_user')
            ->join('sectores AS S', 'S.id_gerencia', 'gerencias.id')
            ->get();

        return view('gestion_gerencias', ['gerencias' => $gerencias]);
    }

    public function getGestionEventos(){

        if(Auth::user() == null){ return abort(403, 'Accion no autorizada');}
        if(Auth::user()->id_role != 1 && Auth::user()->id_role != 2){ return abort(403, 'Accion no autorizada'); }

        $id = Auth::user()->id;

        if(Auth::user()['id_role'] == 1){
            $events = DB::table('eventos')
                ->select('eventos.*', 'G.nombre AS g_nombre')
                ->join('gerencias AS G', 'G.id', 'id_gerencia')
                ->get();
        }elseif(Auth::user()['id_role'] == 2){
            $events = DB::table('eventos')
                ->select('eventos.*', 'G.nombre AS g_nombre')
                ->join('gerencias AS G', 'G.id', 'id_gerencia')
                ->join('users AS U', 'U.id', 'G.id_user')
                ->where('U.id', $id)
                ->get();
        }

        return view('gestion_eventos', ['events' => $events]);
    }

    public function crearAdmin($edit = false, $insert = false){
        if(Auth::user()->id_role != 1){
            return abort(403, 'Accion no autorizada');
        }

        if($edit){
            if($insert){
                echo "<script>alert('Admin creado con éxito');</script>";
            } else {
                echo "<script>alert('No se pudieron insertar los datos');</script>";
            }
        }

        return view('/crear_admin');
    }

    public function createAdmin(Request $request){

        if(Auth::user()->id_role != 1){
            return abort(403, 'Accion no autorizada');
        }

        $id = Auth::user()->id;
        $nombre = $request->form[1]['value'];
        $email = $request->form[2]['value'];
        $cedula = $request->form[3]['value'];
        $password = $request->form[4]['value'];

        $insert = DB::table('users')
            ->insert([
                    'name' => $nombre,
                    'email' =>$email,
                    'cedula' => $cedula,
                    'id_role' => 1,
                    'password' => Hash::make($password),
                ]
            );

        if($insert){
            return response()->json([
                'Done' => true,
                'Status' => 'Success',
                'Message' => 'El administrador fue creado exitosamente.',
                'Data' => null,
                'Header' => 200
            ]);
        }else {
            return response()->json([
                'Done' => false,
                'Status' => 'Error',
                'Message' => 'No se pudo crear el administrador.',
                'Data' => null,
                'Header' => 202
            ]);
        }
    }

    public function getEditarAdmin($id, $edit = false, $userE = false) {
        if(Auth::user()->id_role != 1){
            return abort(403, 'Accion no autorizada');
        }

        $user = DB::table('users')
            ->select('*')
            ->where('id', $id)
            ->first();
        if($edit){
            if($userE){
                echo "<script>alert('Admin editado con éxito');</script>";
            } else {
                echo "<script>alert('No se pudo editar el admin');</script>";
            }
        }

        return view('editar_admin', ['user' => $user]);
    }

    public function getValUser($cedula, $codigo, $token){
        $userExist = DB::table('usuarios_comunes')
            ->select('*')
            ->where('cod_persona', $codigo)
            ->where('cedula', $cedula)
            ->first();

        $boolUser = ($userExist != null);

        if($boolUser){
            return response()->json([
                'Done' => true,
                'Status' => 'Success',
                'Message' => '',
                'Data' => $userExist->id,
                'Header' => 200
            ]);
        }else {
            return response()->json([
                'Done' => false,
                'Status' => 'Error',
                'Message' => 'Usuario no encontrado.',
                'data' => null,
                'Header' => 202
            ]);
        }
    }

    public function registrarse(Request $request){
        $registerExist = DB::table('users_eventos')
            ->select('*')
            ->where('id_user', $request->u_i)
            ->first();
 
        $boolRegister = ($registerExist != null);

        $step = 1;

        return view('registrarse', ['registered' => $boolRegister, 'step' => $step, 'id' => $request->u_i]);
    }

    public function getCupo($step, $datos, $token)
    {
        $datos = explode("&",$datos);

        $data = [];
        $message = '';
        $boolUser = false;
        $asistationId = 0;
        $consultEvent = false;

        if($step == 1){
            $id = explode("=",$datos[0]);
            $registered = explode("=",$datos[1]);
            $id = $id[1];
            $registered = $registered[1]*1;
            
            if(!$registered){
                $userExist = DB::table('usuarios_comunes AS UC')
                    ->select('UC.id AS uc_id', 'UC.nombre AS uc_nombre', 'G.id AS g_id', 'G.nombre AS g_nombre', 'S.nombre AS s_nombre', 'U.name')
                    ->join('gerencias AS G', 'G.cod', 'UC.cod_gerencia')
                    ->join('sectores AS S', 'S.id_gerencia', 'G.id')
                    ->join('users AS U', 'U.id', 'G.id_user')
                    ->where('UC.id', $id)
                    ->first();
            } else {
                $consultEvent = true;
                $asistationId = $id;
                $userExist = true;
            }

            $boolUser = true;
            
            $data = [
                'boolLastStep' => false,
                'boolUser' => $userExist,
                'boolRegister' => $registered
            ];
        }

        if ($step == 2){
            $getPresentialEvent = null;
            $getVirtualEvent = null;
            $presencial = 0; $virtual = 0;
            $correo = explode("=",$datos[0]); $telefono = explode("=",$datos[1]); $divulgacion = explode("=",$datos[2]); $idUser = explode("=",$datos[3]); $idGerencia = explode("=",$datos[4]);
            $correo = $correo[1]; $telefono = $telefono[1]; $divulgacion = $divulgacion[1]; $idUser = $idUser[1]; $idGerencia = $idGerencia[1];


            $userExist = DB::table('usuarios_comunes')
                ->select('*')
                ->where('id', $idUser)
                ->first();

            $duplicatedUser = DB::table('usuarios_comunes')->select('*')->where('email', $correo)->first();
            if(!isset($duplicatedUser->id)){ $duplicatedUser = DB::table('users')->select('*')->where('email', $correo)->first(); }

            if ($userExist->evento_presencial){
                $presencial = 1;
                $getPresentialEvent = DB::table('eventos')->select('*')->where('id_gerencia', $idGerencia)->where('id_tipo_evento', 1)->first();
                if(!isset($getPresentialEvent->id)){ $getPresentialEvent = null; }
            }

            if ($userExist->evento_virtual){
                $virtual = 1;
                $getVirtualEvent = DB::table('eventos')->select('*')->where('id_gerencia', $idGerencia)->where('id_tipo_evento', 2)->first();
                if(!isset($getVirtualEvent->id)){ $getVirtualEvent = null; }
            }

            if(isset($duplicatedUser->id)){
                $boolUser = false;
                $message = 'Lo sentimos, El correo ingresado ya pertenece a un usuario';
            } elseif($getPresentialEvent == null && $getVirtualEvent == null){
                $boolUser = false;
                $message = 'Lo sentimos, No hay eventos para ti actualmente';
            } else {
                DB::table('usuarios_comunes')
                    ->where('id', $idUser)
                    ->update([
                        'email' => $correo,
                        'telefono' => $telefono,
                        'divulgacion_datos' => ($divulgacion)? 1 : 0
                    ]);

                $data = [
                    'boolLastStep' => false,
                    'boolUser' => $userExist,
                    'presencial' => $presencial,
                    'virtual' => $virtual,
                    'getPresentialEvent' => $getPresentialEvent,
                    'getVirtualEvent' => $getVirtualEvent,
                ];

                $boolUser = true;
            }

        }

        if($step == 3) {
            $evento = explode("=",$datos[0]); $idUser = explode("=",$datos[1]);
            $evento = $evento[1]; $idUser = $idUser[1];

            $boolUser = DB::table('users_eventos')
                ->insert([
                    'id_user' => $idUser,
                    'id_evento' => $evento
                ]);

            $consultEvent = $boolUser;
            $asistationId = $idUser;

        }

        if($consultEvent){
            $eventAsistation = DB::table('users_eventos')
                ->select('E.*')
                ->join('eventos AS E', 'E.id', 'id_evento')
                ->where('id_user', $asistationId)
                ->first();

            $data = [
                'boolLastStep' => true,
                'dataEvent' => $eventAsistation
            ];
        }

        if($boolUser){
            return response()->json([
                'Done' => true,
                'Status' => 'Success',
                'Message' => 'Se ha reservado el cupo exitosamente.',
                'Data' => $data,
                'Header' => 200
            ]);
        }else {
            return response()->json([
                'Done' => false,
                'Status' => 'Error',
                'Message' => $message,
                'data' => null,
                'Header' => 202
            ]);
        }   
    }

    public function editAdmin(Request $request) {
        if(Auth::user()->id_role != 1){
            return abort(403, 'Accion no autorizada');
        }

        $nombre = $request->form[1]['value'];
        $id = $request->form[2]['value'];
        $email = $request->form[3]['value'];
        $cedula = $request->form[4]['value'];

        $update = DB::table('users')
            ->where('id', $id)
            ->update([
                    'name' => $nombre,
                    'email' =>$email,
                    'cedula' => $cedula,
                ]
            );

        if($update){
            return response()->json([
                'Done' => true,
                'Status' => 'Success',
                'Message' => 'El administrador fue editado exitosamente.',
                'Data' => null,
                'Header' => 200
            ]);
        }else {
            return response()->json([
                'Done' => false,
                'Status' => 'Error',
                'Message' => 'No se pudo editar el administrador.',
                'Data' => null,
                'Header' => 202
            ]);
        }
    }

    public function getCrearGerecia($edit = false, $insertS = false){
        if(Auth::user()->id_role != 1){
            return abort(403, 'Accion no autorizada');
        }


        if($edit){
            if($insertS){
                echo "<script>alert('Gerencia creada con éxito');</script>";
            } else {
                echo "<script>alert('No se pudieron insertar los datos');</script>";
            }
        }

        return view('/crear_gerencia');
    }

    public function createGerency(Request $request){

        if(Auth::user()->id_role != 1){
            return abort(403, 'Accion no autorizada');
        }

        $id = Auth::user()->id;
        $nombre = $request->form[1]['value'];
        $email = $request->form[2]['value'];
        $cedula = $request->form[3]['value'];
        $password = $request->form[4]['value'];
        $nombreGerencia = $request->form[5]['value'];
        $codGerencia = $request->form[6]['value'];
        $nombreSector = $request->form[7]['value'];
        $codSector = $request->form[8]['value'];

        $insertU = false;
        $insertS = false;

        $insertU = DB::table('users')
            ->insertGetId([
                    'name' => $nombre,
                    'email' =>$email,
                    'cedula' => $cedula,
                    'id_role' => 2,
                    'password' => Hash::make($password),
                ]);

        if($insertU) {
            $insertG = DB::table('gerencias')
                ->insertGetId([
                    'nombre' => $nombreGerencia,
                    'cod' => $codGerencia,
                    'id_user' => $insertU
                ]);
        }

        if($insertG){
            $insertS = DB::table('sectores')
                ->insert([
                    'id_gerencia' => $insertG,
                    'nombre' => $nombreSector,
                    'cod' => $codSector
                ]);
        }

        $retorno = ($insertU || $insertS || $insertS);

        if($retorno){
            return response()->json([
                'Done' => true,
                'Status' => 'Success',
                'Message' => 'La gerencia fue creada exitosamente.',
                'Data' => null,
                'Header' => 200
            ]);
        }else {
            return response()->json([
                'Done' => false,
                'Status' => 'Error',
                'Message' => 'No se pudo crear la gerencia.',
                'Data' => null,
                'Header' => 202
            ]);
        }
    }

    public function getEditarGerency($id, $edit = false, $userE = false) {
        if(Auth::user()->id_role != 1){
            return abort(403, 'Accion no autorizada');
        }

        $gerencia = DB::table('gerencias')
            ->select('gerencias.*', 'U.id AS u_i', 'U.name', 'U.cedula', 'U.email', 'S.id AS s_i', 'S.nombre AS sector_name', 'S.cod AS sector_cod')
            ->join('users AS U', 'U.id', 'id_user')
            ->join('sectores AS S', 'S.id_gerencia', 'gerencias.id')
            ->where('gerencias.id', $id)
            ->first();

        if($edit){
            if($userE){
                echo "<script>alert('Gerencia editada con éxito');</script>";
            } else {
                echo "<script>alert('No se pudo editar la gerencia');</script>";
            }
        }

        return view('editar_gerencia', ['gerencia' => $gerencia]);
    }

    public function editGerency(Request $request) {
        if(Auth::user()->id_role != 1){
            return abort(403, 'Accion no autorizada');
        }

        $nombre = $request->form[1]['value'];
        $id = $request->form[2]['value'];
        $ui = $request->form[3]['value'];
        $si = $request->form[4]['value'];
        $email = $request->form[5]['value'];
        $cedula = $request->form[6]['value'];
        $nombreGerencia = $request->form[7]['value'];
        $nombreSector = $request->form[8]['value'];

        $userE = DB::table('users')
            ->where('id', $ui)
            ->update([
                    'name' => $nombre,
                    'email' =>$email,
                    'cedula' => $cedula,
                ]
            );

        $gerencyE = DB::table('gerencias')
            ->where('id', $id)
            ->update([ 'nombre' => $nombreGerencia ]
            );

        $sectorE = DB::table('sectores')
            ->where('id', $si)
            ->update([ 'nombre' => $nombreSector ]
            );

        $retorno = ($userE || $gerencyE || $sectorE);


        if($retorno){
            return response()->json([
                'Done' => true,
                'Status' => 'Success',
                'Message' => 'La gerencia fue editada exitosamente.',
                'Data' => null,
                'Header' => 200
            ]);
        }else {
            return response()->json([
                'Done' => false,
                'Status' => 'Error',
                'Message' => 'No se pudo editar la gerencia.',
                'Data' => null,
                'Header' => 202
            ]);
        }
    }

    public function getTyC(){
        return view('tyc');
    }
}