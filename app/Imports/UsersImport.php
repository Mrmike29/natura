<?php

namespace App\Imports;

use App\UsuariosComunes;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Support\Facades\DB;

class UsersImport implements ToCollection
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */

    public function collection(Collection $rows)
    {

        $datosExcel = array();

        for($i = 2; $i < count($rows); $i++){
            if($rows[$i][0] != null){
                for ($j = 0; $j < 12; $j++){
                    $datosExcel[$i][$j] = $rows[$i][$j];
                }
                if($rows[$i][12] != null){ $datosExcel[$i][12] = 1; }
                else { $datosExcel[$i][12] = 0; }

                if($rows[$i][15] != null){ $datosExcel[$i][13] = 1;}
                else { $datosExcel[$i][13] = 0;}
            }
        }

        foreach ($datosExcel as $row) {

            $userExist =
                DB::table('usuarios_comunes')->select(DB::raw('count(id) as user_count'))->where('cod_persona', $row[0])->orWhere('cedula', $row[1])->first();
            $gerencia = DB::table('gerencias')->select('id')->where('cod', $row[4])->first();

            if(isset($gerencia->id)){

                if($userExist->user_count > 0){
                    $userExist = DB::table('usuarios_comunes')->select('id')->where('cod_persona', $row[0])->first();
                    DB::table('usuarios_comunes')
                        ->where('id', $userExist->id)
                        ->update([
                            'nombre' => $row[2],
                            'nivel' => $row[3],
                            'gdn' => $row[8],
                            'ciclo' => $row[10],
                            'perfil_activo' => $row[11],
                            'evento_presencial' => $row[12],
                            'evento_virtual' => $row[13],
                        ]);
                } else {
                    UsuariosComunes::create([
                        'cod_persona' => $row[0],
                        'cedula' => $row[1],
                        'nombre' => $row[2],
                        'nivel' => $row[3],
                        'cod_gerencia' => $row[4],
                        'gerencia' => $row[5],
                        'cod_sector' => $row[6],
                        'sector' => $row[7],
                        'gdn' => $row[8],
                        'cod_grupo' => $row[9],
                        'ciclo' => $row[10],
                        'perfil_activo' => $row[11],
                        'id_gerencia' => $gerencia->id,
                        'evento_presencial' => $row[12],
                        'evento_virtual' => $row[13],
                    ]);
                }
            }
        }
    }
}
