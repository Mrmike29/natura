<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Auth;

Auth::routes();

Route::get('/', function () { return redirect('/home'); });

Route::get('/home', 'MainController@index');
Route::get('/gestion', 'MainController@getGestion');
Route::get('/tyc', 'MainController@getTyC');
Route::get('/eventos', 'MainController@eventos')->middleware(['auth', 'can:home']);
Route::get('/mis-usuarios', 'MainController@misUsuarios')->middleware(['auth', 'can:home']);
Route::get('/cargar-usuarios', 'MainController@cargarUsuarios');
Route::post('upload', 'MainController@upload')->name('upload');
Route::get('/crear-evento/{id?}', 'MainController@crearEvento');
Route::post('createEvent', 'MainController@createEvent')->name('createEvent');

Route::get('solicitar-cupo/{step}/{data}/{token}', 'MainController@getCupo');

Route::get('/descargar-listado/{id}', 'MainController@donwloadEventUsers');

Route::get('/gestion-administradores', 'MainController@getGestionAdministradores');
Route::get('/gestion-gerencias', 'MainController@getGestionGerencias');
Route::get('/gestion-eventos', 'MainController@getGestionEventos');

Route::get('/crear-administrador', 'MainController@crearAdmin');
Route::post('createAdmin', 'MainController@createAdmin')->name('createAdmin');
Route::get('/editar-admin/{id}', 'MainController@getEditarAdmin');
Route::post('editAdmin', 'MainController@editAdmin')->name('editAdmin');
Route::get('/crear-gerencia', 'MainController@getCrearGerecia');
Route::post('createGerency', 'MainController@createGerency')->name('createGerency');
Route::get('/editar-gerencia/{id}', 'MainController@getEditarGerency');
Route::post('editGerency', 'MainController@editGerency')->name('editGerency');

Route::get('/validate-user-exist/{cedela}/{codigo}/{token}', 'MainController@getValUser');
Route::post('registrarse', 'MainController@registrarse')->name('registrarse'); 

